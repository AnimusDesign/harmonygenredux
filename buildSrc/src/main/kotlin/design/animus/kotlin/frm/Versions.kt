package design.animus.kotlin.frm

object Versions {
    object Plugins {
        const val kotlin = Dependencies.kotlin
        const val gradlePluginPublish = "0.10.1"
    }

    object Dependencies {
        const val kotlin = "1.3.61"
        const val serialization = "0.14.0"
        const val coroutine = "1.3.3"
        const val vertx = "4.0.0-milestone4"
        const val postgreSQL = "42.2.5"
        const val junit = "4.12"
        const val jasync = "1.0.12"
        const val kotlinLogging = "1.7.9-SNAPSHOT"
        const val logback = "1.2.3"
        const val MPDataTypes = "0.1.2"
        const val mariaDBDriver = "2.5.2"
        const val frmVersion = "0.1.7-SNAPSHOT"
        const val micrometer = "1.4.1"
        const val fabricK8 = "4.9.1"
        const val kubernetesDSL = "2.7"
        const val sentry = "1.7.30"
    }
}