import java.net.URI

enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "musicbrainz-demo"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.contains("org.flywaydb.flyway")) {
                useVersion("6.0.7")
            }
            if (requested.id.id.startsWith("org.jetbrains.dokka")) {
                // Update Version Build Source if being changed.
                useVersion("0.10.0")
            }
            if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization") ) {
                // Update Version Build Source if being changed.
                useVersion("1.3.72")
            }
            if (requested.id.id.startsWith("design.animus.kotlin.frm.sql.schema")) {
                // Update Version Build Source if being changed.
                useVersion("0.1.7-SNAPSHOT")
            }
        }
    }
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
        gradlePluginPortal()
        maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }

    }
}

// Music Brainz Services
//include(":services:musicbrainz:postgres:")
//include(":services:musicbrainz:records:")
include(":services:musicbrainz:api:")
include(":services:musicbrainz:deploy:")