import java.net.URI

allprojects {
    buildscript {
        repositories {
	maven { url = URI("https://animusdesign-repository.appspot.com") }
            mavenCentral()
            mavenLocal()
            jcenter()
            google()
            maven { url = URI("https://kotlin.bintray.com/kotlinx") }
            maven { url = URI("https://dl.bintray.com/kotlin/kotlin-js-wrappers") }
            maven { url = URI("https://kotlin.bintray.com/kotlinx") }
            maven { url = URI("https://maven.google.com") }
            maven { url = URI("https://plugins.gradle.org/m2/") }
            maven { url = URI("http://dl.bintray.com/kotlin/kotlin-dev") }
            maven { url = URI("https://dl.bintray.com/kotlin/kotlin-eap") }
        }
    }


    repositories {
	maven { url = URI("https://animusdesign-repository.appspot.com") }
        mavenCentral()
        jcenter()
        google()
        maven { url = URI("https://kotlin.bintray.com/kotlinx") }
        maven { url = URI("https://dl.bintray.com/kotlin/kotlin-js-wrappers") }
        maven { url = URI("https://kotlin.bintray.com/kotlinx") }
        maven { url = URI("https://maven.google.com") }
        maven { url = URI("https://plugins.gradle.org/m2/") }
        maven { url = URI("http://dl.bintray.com/kotlin/kotlin-dev") }
        maven { url = URI("https://dl.bintray.com/kotlin/kotlin-eap") }
        mavenLocal()
    }

    allprojects.filter { it.name.contains("services") }.forEach { project ->
        project.subprojects {
            apply("$rootDir/versions.gradle.kts")
            //apply("$rootDir/gradle/common/publishing-repos.gradle.kts")
            apply(plugin = "maven-publish")
            group = "${extra["baseGroup"]}.services"
            version = "${extra["projectVersion"]}"
        }
    }
}

