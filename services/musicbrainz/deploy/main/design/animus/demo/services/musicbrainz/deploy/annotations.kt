package design.animus.demo.services.musicbrainz.deploy

val gitLabAnnotations = mutableMapOf<String, String>(
        "app.gitlab.com/app" to EnvConfig.ciProjectPathSlug,
        "app.gitlab.com/env" to EnvConfig.ciEnvironmentSlug,
        "kubernetes.io/ingress.class" to "nginx",
        "kubernetes.io/tls-acme" to "true"
)

val ingressAnnotations = mutableMapOf(
        "nginx.ingress.kubernetes.io/proxy-body-size" to "50m",
        "nginx.org/client-max-body-size" to "50m",
        "ingress.kubernetes.io/proxy-body-size" to "50m",
        "kubernetes.io/ingress.class" to "nginx",
        "kubernetes.io/tls-acme" to "true",
        "nginx.ingress.kubernetes.io/client-body-buffer-size" to "50m",
        "nginx.org/proxy-connect-timeout" to "30s",
        "nginx.org/proxy-read-timeout" to "20s"
)
