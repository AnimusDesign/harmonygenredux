package design.animus.demo.services.musicbrainz.deploy


import com.fkorotkov.kubernetes.*
import com.fkorotkov.kubernetes.apps.metadata
import com.fkorotkov.kubernetes.apps.selector
import com.fkorotkov.kubernetes.apps.spec
import com.fkorotkov.kubernetes.apps.template
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.Service
import io.fabric8.kubernetes.api.model.apps.Deployment

const val baseAPIName = "$applicationName-musicbrainz-api"

open class APIService(private val serviceName: String = baseAPIName) : Service() {

    companion object {
        val apiPort = EnvConfig.apiPort.toInt()
        const val portName = "musicbrainz-api-port"
    }

    init {
        metadata {
            name = serviceName
            labels = applicationLabels
            annotations = gitLabAnnotations + ingressAnnotations + mutableMapOf(
                    "prometheus.io/scrape" to "true",
                    "prometheus.io/path" to "/metrics",
                    "prometheus.io/port" to "$apiPort"
            )
        }
        spec {
            selector = applicationLabels
            ports = listOf(
                    newServicePort {
                        port = apiPort
                        name = portName
                        targetPort = IntOrString(apiPort)
                    }
            )
            clusterIP = "None"
        }
    }
}


class APIDeployment(serviceName: String = baseAPIName) : Deployment() {
    init {
        metadata {
            name = serviceName
            annotations = gitLabAnnotations + ingressAnnotations
            labels = applicationLabels
        }
        spec {
            replicas = 1
            selector {
                matchLabels = applicationLabels
            }
            template {
                metadata {
                    labels = applicationLabels
                }
                spec {
                    containers = listOf(
                            newContainer {
                                name = baseAPIName
                                image = apiImage
                                imagePullPolicy = "Always"
                                ports = listOf(
                                        newContainerPort {
                                            containerPort = APIService.apiPort
                                        }
                                )
                                env = listOf(
                                        newEnvVar {
                                            name = "apiPort"
                                            value = "$APIService.apiPort"
                                        },
                                        newEnvVar {
                                            name = "SentryURL"
                                            value = System.getenv("SentryURL")
                                        }
                                )
                            }
                    )
                }
            }
        }
    }
}

