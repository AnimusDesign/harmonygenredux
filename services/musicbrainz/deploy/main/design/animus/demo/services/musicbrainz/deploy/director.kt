package design.animus.demo.services.musicbrainz.deploy

import com.fkorotkov.kubernetes.extensions.*
import design.animus.demo.services.musicbrainz.deploy.EnvConfig.ciEnvironmentSlug
import design.animus.demo.services.musicbrainz.deploy.EnvConfig.ciProjectPathSlug
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.client.DefaultKubernetesClient


const val applicationName = "animus-design"
val applicationLabels = mapOf(
        "app" to applicationName
)

val apiImage = "registry.gitlab.com/animusdesign/harmonygenredux/musicbrainz-api:${System.getenv("CI_COMMIT_SHORT_SHA")}"
val nameSpace = System.getenv("KUBE_NAMESPACE") ?: "harmonygenredux"

// GitLab Specific Elements
object EnvConfig {
    val ciProjectPathSlug = System.getenv("CI_PROJECT_PATH_SLUG") ?: ""
    val ciEnvironmentSlug = System.getenv("CI_ENVIRONMENT_SLUG") ?: ""
    val apiPort = System.getenv("MusicBrainzAPIPort") ?: "8080"
    val envUrl = System.getenv("CI_ENVIRONMENT_URL") ?: "https://blog.animus.design"
    val hostName = envUrl
            .replace("https://", "")
            .replace("https://", "")
}


suspend fun main() {
    println("======================")
    println("Deploying Kotlin K8 DSL Musicbrainz API Instance")
    println("NameSpace: $nameSpace")
    println("CI Project Path Slug: $ciProjectPathSlug")
    println("CI Environment Path Slug: $ciEnvironmentSlug")
    val client = DefaultKubernetesClient()
    println("Creating MusicBrainz API Deployment")
    client.apps().deployments().inNamespace(nameSpace).createOrReplace(APIDeployment())

    println("Creating Blog Service")
    client.services().inNamespace(nameSpace).createOrReplace(APIService())
    println("Creating Blog Deployment")
    client.apps().deployments().inNamespace(nameSpace).createOrReplace(APIDeployment())
    println("Creating Ingress")
    client.inNamespace(nameSpace).extensions().ingresses().createOrReplace(
            newIngress {
                metadata {
                    name = "$applicationName-ingress"
                    annotations = gitLabAnnotations + ingressAnnotations

                }
                spec {
                    tls = listOf(
                            newIngressTLS {
                                hosts = listOf(
                                        EnvConfig.hostName
                                )
                                secretName = "$nameSpace-$applicationName-tls-secret"
                            }
                    )
                    rules = listOf(
                            newIngressRule {
                                host = EnvConfig.hostName
                                http = newHTTPIngressRuleValue {
                                    paths = listOf(
                                            newHTTPIngressPath {
                                                backend = newIngressBackend {
                                                    serviceName = baseAPIName
                                                    servicePort = IntOrString(APIService.apiPort)
                                                }
                                            }
                                    )
                                }
                            }
                    )
                }
            }
    )

}