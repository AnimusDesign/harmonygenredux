import design.animus.kotlin.frm.Versions.Dependencies

plugins {
    kotlin("jvm")
    application
}

repositories {
    mavenCentral()
    jcenter()
}
kotlin {
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("io.fabric8:kubernetes-client:${Dependencies.fabricK8}")
                implementation("com.fkorotkov:kubernetes-dsl:${Dependencies.kubernetesDSL}")
            }
        }
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

application {
    mainClassName = "design.animus.demo.services.musicbrainz.deploy.DirectorKt"
}
