import design.animus.kotlin.frm.Versions.Dependencies
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm")
    id("com.github.johnrengelman.shadow") version "5.2.0"
}

kotlin {
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
//                implementation(project(":services:musicbrainz:postgres:"))
                implementation("io.vertx:vertx-core:${Dependencies.vertx}")
                implementation("io.vertx:vertx-lang-kotlin:${Dependencies.vertx}")
                implementation("io.vertx:vertx-lang-kotlin-coroutines:${Dependencies.vertx}")
                implementation("io.vertx:vertx-web:${Dependencies.vertx}")
                implementation("io.vertx:vertx-health-check:${Dependencies.vertx}")
//                implementation("io.vertx:vertx-pg-client:${Dependencies.vertx}")
                implementation("io.vertx:vertx-opentracing:${Dependencies.vertx}")
                implementation("io.vertx:vertx-micrometer-metrics:4.0.0-milestone4")
                implementation("io.micrometer:micrometer-registry-prometheus:${Dependencies.micrometer}")
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
                implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.postgresql:postgresql:${Dependencies.postgreSQL}")
                implementation("io.sentry:sentry-logback:${Dependencies.sentry}")
//                implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
//                implementation("design.animus.kotlin.frm.sql:vertx-base:${Dependencies.frmVersion}")
//                implementation("design.animus.kotlin.frm.sql:vertx-postgres:${Dependencies.frmVersion}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
            }
        }
    }
}


tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Test>().configureEach {
    useJUnit()
    reports.junitXml.isEnabled = true
}


tasks {
    named<ShadowJar>("shadowJar") {
        archiveBaseName.set("musicbrainz-api")
        mergeServiceFiles()
        manifest {
            attributes(mapOf("Main-Class" to "design.animus.demo.services.musicbrainz.api.DirectorKt"))
        }
    }
}

tasks {
    build {
        dependsOn(shadowJar)
    }
}