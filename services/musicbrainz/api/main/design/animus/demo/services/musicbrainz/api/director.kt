package design.animus.demo.services.musicbrainz.api

import io.micrometer.core.instrument.Metrics
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics
import io.micrometer.core.instrument.binder.system.ProcessorMetrics
import io.micrometer.prometheus.PrometheusMeterRegistry
import io.sentry.Sentry
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.web.Route
import io.vertx.ext.web.Router.router
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.dispatcher
import io.vertx.micrometer.MicrometerMetricsOptions
import io.vertx.micrometer.PrometheusScrapingHandler
import io.vertx.micrometer.VertxPrometheusOptions
import io.vertx.micrometer.backends.BackendRegistries
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mu.KotlinLogging

fun Route.coroutineHandler(fn: suspend (RoutingContext) -> Unit) {
    handler { ctx ->
        GlobalScope.launch(ctx.vertx().dispatcher()) {
            try {
                fn(ctx)
            } catch (e: Exception) {
                ctx.fail(e)
            }
        }
    }
}

val logger = KotlinLogging.logger {  }

suspend fun main() {
    Sentry.init(System.getenv("SentryURL"))
    val vertx = Vertx.vertx(VertxOptions().apply {
        metricsOptions = MicrometerMetricsOptions().apply {
            prometheusOptions = VertxPrometheusOptions().apply { isEnabled = true }
            isEnabled = true
        }
    })
    logger.error { "Test error" }
    val registry = BackendRegistries.getDefaultNow() as PrometheusMeterRegistry
    ClassLoaderMetrics().bindTo(registry)
    JvmMemoryMetrics().bindTo(registry)
    JvmGcMetrics().bindTo(registry)
    ProcessorMetrics().bindTo(registry)
    JvmThreadMetrics().bindTo(registry)
    val healthCheck = HealthCheckHandler.create(vertx)
    val server = vertx.createHttpServer()
    val router = router(vertx)
    router.route("/health").handler(healthCheck)
    router.route("/metrics").handler(PrometheusScrapingHandler.create())
    router.get("/").coroutineHandler { routingContext ->

        registry.timer("httpResponseTime").record {
            routingContext.response().end("Hello")
        }
    }
    server.requestHandler(router).listen(8080)


}